# Site Snapper 

This is a simple application that uses Watir to build a gallery 
of screenshots for a predefined set of pages in a site.

## Quickstart

- Install Ruby
- Install Bundler gem (`gem install bundler`)
- Clone git repository (or download and unzip)
- Run `bundle` in project directory
- Copy config.yml.sample to config.yml
- Populate config.yml with your details.  The two main areas to change are the `prefix` url and array of `pages` to scan. 
- Run the code `ruby run.rb`

## TODOs

- refactor out the config.yml and move it to a separate profiles project
  - site-snapper-profiles

