require 'watir-webdriver'
require 'yaml'

config = YAML.load_file('config.yml')

# create save_dir if !exists
unless FileTest::directory?(config['save_dir'])
  Dir::mkdir(Dir::pwd + "/" + config['save_dir'])
end

browser = Watir::Browser.new

total_pages = config['pages'].size
idx = 0
config['pages'].each do |page|
  puts "Processing #{idx}/#{total_pages}: #{page}"
  browser.goto config['prefix'] + page
  browser.screenshot.save "#{config['save_dir']}/#{page.gsub(/\//,'-')}.png"
  idx += 1
end

puts "Screenshots saved to #{Dir::pwd}/#{config['save_dir']}"

browser.close

